import React from 'react';
import ReactDOM from 'react-dom';
import AppNavbar from './components/global/AppNavbar.jsx';
import App from './core/App';

window.App = App;
App.init();
