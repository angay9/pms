import Provider from './Provider';
import injectTapEventPlugin from 'react-tap-event-plugin';

var MaterialUIProvider = Object.create(Provider);

MaterialUIProvider.register = function (app) {
    // Material UI
    injectTapEventPlugin();
};

export default MaterialUIProvider;