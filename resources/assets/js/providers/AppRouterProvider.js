import Provider from './Provider';
import AppRouter from '../routing/AppRouter.js';

var AppRouterProvider = Object.create(Provider);

AppRouterProvider.register = function (app) {
    // App router
    let router = AppRouter.init();
    window.router = router;
};

export default AppRouterProvider;