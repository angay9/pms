import Provider from './Provider';
var JQueryProvider = Object.create(Provider);

JQueryProvider.register = function (app) {
    // jQuery $.ajax
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content'),
        },
        beforeSend: function(xhr, options) {
            options.url = '/app/' + options.url;
            options.url = options.url.replace('//', '/');
        }
    });

};
export default JQueryProvider;