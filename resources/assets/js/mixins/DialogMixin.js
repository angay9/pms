var AlertMixin = {
   
    alert(message, type, autoHideTime) {
        this.props.appComponent.alert(message, type, autoHideTime);
    },

    confirm(title, message, yesCallback, noCallback) {
        this.props.appComponent.confirm(title, message, yesCallback, noCallback);
    }
};
export default AlertMixin;