var FormErrorsMixin = {
    getInitialState: () => {
        return {
            errors: {},
        };
    },

    formErrors: function (field, separator) {
        var errors = this.state.errors;
        if (!errors) {
            return '';
        }
        var separator = separator ? '. ' : '';

        if (errors[field] !== undefined) {
            return errors[field].join(separator);
        }

        return '';
    }
};

export default FormErrorsMixin;