var FormSubmitMixin = {
    submit: function (url, data, successCallback, errCallback) {
        var successCallback = successCallback ? successCallback : new Function ();
        var errCallback = errCallback ? errCallback : new Function ();

        $.post(url.replace(/^\/|\/$/g, ''), data, function (resp) {
            successCallback(resp);
            this.setState({errors: {}});
        }.bind(this)).error(function (errResp) {
            try {
                var errors = errResp.responseJSON;
                this.setState({errors: errors});
            } catch (e) {
            } finally {
                errCallback(errResp);
            }
        }.bind(this));
    }
};

export default FormSubmitMixin;