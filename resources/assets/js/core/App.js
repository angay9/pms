import JQueryProvider from '../providers/JQueryProvider';
import MaterialUIProvider from '../providers/MaterialUIProvider';
import AppRouterProvider from '../providers/AppRouterProvider';

var App = (function () {
    return {
        providers: [
            JQueryProvider,
            MaterialUIProvider,
            AppRouterProvider
        ],
        init: function (appComponent) {

            for (let provider of this.providers) 
            {
                provider.register(this);
            }
        }
    }
})();

export default App;