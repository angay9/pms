import { Router, Route, Link, browserHistory } from 'react-router';
import React from 'react';
import ReactDOM from 'react-dom';
import AppComponent from '../components/global/AppComponent.jsx';
import ClientList from '../components/clients/ClientList.jsx';
import ClientForm from '../components/clients/ClientForm.jsx';

import ProjectList from '../components/projects/ProjectList.jsx';
import ProjectForm from '../components/projects/ProjectForm.jsx';

import TaskList from '../components/projects/tasks/TaskList.jsx';
import TaskForm from '../components/projects/tasks/TaskForm.jsx';
import TaskView from '../components/projects/tasks/TaskView.jsx';

import Settings from '../components/settings/Settings.jsx';

var AppRouter = {

    init: function () {
        return ReactDOM.render(
            <Router history={browserHistory}>
                <Route path="/" component={AppComponent}>
                    <Route path="/client" components={ClientList}>
                    </Route>
                    <Route path="/client/create" components={ClientForm}>
                    </Route>
                    <Route path="/client/:clientId/edit" components={ClientForm}>
                    </Route>

                    <Route path="/project" components={ProjectList}>
                    </Route>
                    <Route path="/project/create" components={ProjectForm}>
                    </Route>
                    <Route path="/project/:projectId/edit" components={ProjectForm}>
                    </Route>

                    <Route path="/project/:projectId/tasks" components={TaskList}></Route>
                    <Route path="/project/:projectId/tasks/create" components={TaskForm}></Route>
                    <Route path="/tasks/:taskId/view" components={TaskView}></Route>
                    
                    <Route path="/settings" components={Settings}></Route>
                </Route>
            </Router>
        , document.getElementById('app'));
    }
};

export default AppRouter;