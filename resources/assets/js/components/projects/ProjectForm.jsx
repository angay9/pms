import React from 'react';
import FormErrorsMixin from '../../mixins/FormErrorsMixin';
import FormSubmitMixin from '../../mixins/FormSubmitMixin';
import moment from 'moment';
import Input from '../core/Input.jsx';
import ReactMarkdown from 'react-markdown';


var ProjectForm = React.createClass({
    mixins: [FormErrorsMixin, FormSubmitMixin],
    
    componentWillMount: function () {
        let projectId = this.props.params.projectId;

        if (projectId) {
            $.get('project/' + projectId, function (resp) {
                this.setState({
                    project: resp.project,
                    editMode: true
                });
            }.bind(this));
        }

        $.get('client', {page: 1}, function (response) {
            this.setState({
                clients: response.items,
                clientsPaginator: {
                    totalCount: response.paginator.total_count,
                    totalPages: response.paginator.total_pages,
                    currentPage: response.paginator.current_page,
                    limit: response.paginator.limit
                }
            });
        }.bind(this));
    },

    getInitialState: () => {
        return {
            project: {
                id: '',
                name: '',
                description: '',
                endDate: '',
                clientId: '',
            },
            clients: [],
            clientsPaginator: {},
            editMode: false,
        };
    },

    saveProject: function(e) {
        let url = 'project';
        let data = this.state.project;

        if (this.isEditMode()) {
            url += '/' + data.id;
            data['_method'] = 'PATCH';
        }

        this.submit(url, data, function (resp) {
            // @TODO show snackbar
        }.bind(this), function (errResp) {
            // @TODO show snackbar
        }.bind(this));
    },

    setProjectName: function(e) {
        let project = this.state.project;
        project.name = e.target.value;

        this.setState({
            project: project
        });
    },

    setProjectDescription: function(e) {
        let project = this.state.project;
        project.description = e.target.value;

        this.setState({
            project: project
        });
    },

    setClientId: function(e) {
        let project = this.state.project;
        project.clientId = e.target.value;

        this.setState({
            project: project
        });
    },
    

    openDatepicker: function() {
        this.refs.datepicker.openDialog();
    },
    changeEndDate: function(date) {
        var project = this.state.project;
        // var date = moment(date);
        project.endDate = date;
        // project.endDate = date.format('YYYY-MM-DD');
        this.setState({project: project});
    },

    isEditMode: function () {
        return this.state.editMode;
    },

    render: function() {
        let clientsDataSource = this.state.clients.map((client) => {
            var clientOpt = {};
            clientOpt[client.id] = client.name;
            return clientOpt;
        });

        return (
            <div className="row">
                <div className="col-md-12">
                    <Input 
                        value={this.state.project.name}
                        label="Project name"
                        onChange={this.setProjectName}
                        errors={this.formErrors('name')}
                    />
                    
                    <div role="tabpanel">
                        <ul className="nav nav-tabs" role="tablist">
                            <li role="presentation" className="active">
                                <a href="#description" aria-controls="Description" role="tab" data-toggle="tab">Description</a>
                            </li>
                            <li role="presentation">
                                <a href="#preview" aria-controls="Preview" role="tab" data-toggle="tab">Preview</a>
                            </li>
                        </ul>
                    
                        <div className="tab-content">
                            <div role="tabpanel" className="tab-pane active" id="description">
                                <Input 
                                    type="textarea"
                                    value={this.state.project.description}
                                    label="Project description"
                                    onChange={this.setProjectDescription}
                                    errors={this.formErrors('description')}
                                />
                            </div>
                            <div role="tabpanel" className="tab-pane" id="preview">
                                <ReactMarkdown source={this.state.project.description} />
                            </div>
                        </div>
                    </div>

                    <Input 
                        type="date"
                        value={this.state.project.endDate}
                        label="Project end date"
                        errors={this.formErrors('endDate')}
                        onChange={this.changeEndDate}
                    />
                   
                    <Input
                        type="select"
                        onChange={this.setClientId}
                        errors={this.formErrors('clientId')}
                        label="Client"
                        options={clientsDataSource}
                        value={this.state.project.clientId}
                    />

                    <div className="form-group">
                        <button 
                            onClick={this.saveProject}
                            className="btn btn-primary btn-outline">
                            <i className="fa fa-plus-circle"></i>{' '}
                            Save
                        </button>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = ProjectForm;