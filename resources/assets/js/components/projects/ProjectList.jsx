import React from 'react';
import FloatingActionButton from 'material-ui/lib/floating-action-button';
import ContentAdd from 'material-ui/lib/svg-icons/content/add';
import { Link } from 'react-router';
import { browserHistory } from 'react-router';
import DialogMixin from '../../mixins/DialogMixin';

var ProjectList = React.createClass({
    
    mixins: [DialogMixin],

    componentWillMount: function () {
        this.loadData();
    },

    loadData: function (pageNum) {
        let page = pageNum === undefined ? 1 : pageNum;

        $.get('project', {page: page}, function (response) {
            this.setState({
                projects: response.items,
                paginator: {
                    totalCount: response.paginator.total_count,
                    totalPages: response.paginator.total_pages,
                    currentPage: response.paginator.current_page,
                    limit: response.paginator.limit
                }
            });
        }.bind(this));
    },

    getInitialState: function () {
        return {
            projects: [],
            paginator: {}
        };
    },

    deleteItem: function (project, e) {

        e.preventDefault();
        e.stopPropagation();
        if (!confirm('Are you sure?')) {
            return;
        }

        $.post('project/' + project.id, {'_method': 'DELETE'}, function (resp) {
            let projects = this.state.projects;
            var index = projects.indexOf(project);
            projects.splice(index, 1);
            this.setState({
                projects: projects
            });

        }.bind(this));
    },
    calcProjectCompletePctg(project) {
        if (project.tasksCount == 0) {
            return 0;
        }
        return ((project.completeTasksCount / project.tasksCount) * 100).toFixed(2);
    },

    render: function() {
        let projects = this.state.projects;
        let currentPage = this.state.paginator.currentPage;
        let totalPages = this.state.paginator.totalPages;
        let totalCount = this.state.paginator.totalCount;
        
        return (
                <div>
                    <div className="mbot-10">
                        <Link to="/project/create" className="btn btn-primary btn-outline">
                            <i className="fa fa-plus-circle"></i> Add
                        </Link>
                    </div>
                    <div className="row">    
                        {projects.map((project) => {
                            let projectCompletePctg = this.calcProjectCompletePctg(project);
                            return (
                                <div className="col-md-12" key={project.id}>
                                    <div className="thumbnail">
                                        <div className="well">
                                            <h3 style={{color: '#323232'}}>{project.name}</h3>
                                        </div>
                                        <div className="caption">
                                            <p>
                                                <a href="#" onClick={this.deleteItem.bind(this, project)} className="btn btn-danger btn-outline">
                                                    <i className="fa fa-trash-o"></i>
                                                </a>
                                                {' '}
                                                <Link to={"/project/" + project.id + "/edit"} className="btn btn-success btn-outline">
                                                    <i className="fa fa-pencil"></i>
                                                </Link>
                                                {' '}
                                                <Link to={"/project/" + project.id + "/tasks"} className="btn btn-warning btn-outline">
                                                    <i className="fa fa-sticky-note"></i>
                                                </Link>
                                            </p>
                                            <p>
                                                Tasks: {project.tasksCount}
                                                <br />
                                                Remainig: {project.incompleteTasksCount}
                                            </p>
                                            <div>
                                                <div className="progress">
                                                    <div 
                                                        className="progress-bar progress-bar-primary progress-bar-striped" 
                                                        role="progressbar" 
                                                        aria-valuenow="projectCompletePctg" 
                                                        aria-valuemin="0" 
                                                        aria-valuemax="100" 
                                                        style={{width: projectCompletePctg + '%'}}
                                                    >
                                                        <span>{projectCompletePctg}% Complete</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <ul className="pager">
                                <li className="previous">
                                    <a href="#" disabled={currentPage === 1} onClick={this.loadData.bind(this, currentPage - 1)}>&larr; Prev</a>
                                </li>
                                <li className="next">
                                    <a href="#" disabled={currentPage + 1 > totalPages} onClick={this.loadData.bind(this, currentPage + 1)}>&rarr; Next</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
        );
    }
});


export default ProjectList;