import React from 'react';
import { Link } from 'react-router';
import Input from '../../core/Input.jsx';
import ReactMarkdown from 'react-markdown';
import FormErrorsMixin from '../../../mixins/FormErrorsMixin';
import FormSubmitMixin from '../../../mixins/FormSubmitMixin';
import Avatar from '../../global/Avatar.jsx';
import DialogMixin from '../../../mixins/DialogMixin';

var TaskView = React.createClass({
    mixins: [FormSubmitMixin, FormErrorsMixin, DialogMixin],
    componentWillMount() {
        let taskId = this.props.params.taskId;
        $.get('task/' + taskId, (resp) => {
            
            this.setState({ task: resp.task });
            let task = this.state.task;

            $.get('asignee', {taskId: task.id}, (users) => {
                this.setState({ users });
                if (users.length > 0) {
                    this.setState({ selectedAsigneeId: users[0].id });
                }
            });
        });
    },

    getInitialState() {
        return {
            task: null,
            selectedAsigneeId: null,
            users: null,
            comment: {
                id: null,
                body: ''
            },
            comments: []
        };
    },

    addAsignee: function (asigneeId, taskId, e) {
        e.preventDefault();
        $.post(`asignee/add/${taskId}/${asigneeId}`, (resp) => {
            let users = this.state.users;
            let user = users.filter((user) => user.id == asigneeId)[0];
            let task = this.state.task;

            users.splice(
                users.indexOf(user), 1
            );

            this.setState({
                users: users
            });

            this.setState({
                selectedAsigneeId: users.length > 0 ? users[0].id : null
            });

            task.asignees.push(user);

            this.setState({ task });
        });
    },

    removeAsignee: function (asigneeId, taskId, e) {
        e.preventDefault();
        
        this.confirm('Are you sure?', 'Do you really want to remove this asignee?', () => {
            $.post(`asignee/remove/${taskId}/${asigneeId}`, {'_method': 'DELETE'}, (resp) => {
                let users = this.state.users;
                let task = this.state.task;
                
                let user = task.asignees.filter((user) => user.id == asigneeId)[0];
                
                task.asignees.splice(
                    task.asignees.indexOf(user), 1
                );

                this.setState({ task });

                this.setState({
                    selectedAsigneeId: users.length > 0 ? users[0].id : null
                });

                users.push(user);

                this.setState({ users });      
            });
        });

    },

    saveComment(e) {
        e.preventDefault();
        let taskId = this.state.task.id;
        let url = `task/${taskId}/comment/add`;
        let commentId = this.state.comment.id;
        let data = {
            'body': this.state.comment.body
        };

        if (commentId) {
            url = `task/${taskId}/comment/${commentId}/update`;
            data['_method'] = 'PUT';
        }

        this.submit(url, data, (resp) => {
            // @TODO show snackbar
            let task = this.state.task;
            
            if (!commentId) {
                task.comments.push(resp.comment);    
            } else {
                let updatedComment = task.comments.filter((comment) => comment.id == commentId)[0];
                let index = task.comments.indexOf(updatedComment);
                updatedComment.body = data.body;
                task.comments[index] = updatedComment;
            }
            
            this.setState({task});  
            this.setState({
                comment: {
                    id: '',
                    body: ''
                }
            });
        }, (errResp) => {
            // @TODO show snackbar
        });
    },

    removeComment(comment, e) {
        e.preventDefault();
        e.stopPropagation();

        this.confirm('Are you sure?', 'Do you really want to delete this comment?', () => {
            let taskId = this.state.task.id;
            let commentId = comment.id;

            $.post(`task/${taskId}/comment/${commentId}/remove`, {'_method': 'DELETE'}, (resp) => {

                let task = this.state.task;
                var index = task.comments.indexOf(comment);
                task.comments.splice(index, 1);
                this.setState({
                    task
                });
            });
        });
    },

    updateComment(comment, e) {
        e.preventDefault();
        this.setState({
            comment    
        });
    },

    setCommentBody(e) {
        let comment = this.state.comment;
        let commentExists = this.state.task.comments.indexOf(comment) !== -1;
        if (commentExists) {
            comment = Object.create(comment);
        }
        comment.body = e.target.value;
        this.setState({
            comment
        });
    },

    render: function() {
        let task = this.state.task;
        let users = this.state.users;
        if (!task || !users) { // Ajax request is loading
            return (
                <div className="row">
                    <div className="col-md-12">
                        <p>Loading...</p>
                    </div>
                </div>
            );
        }

        users = this.state.users.map((user) => {
            var userOption = {};
            userOption[user.id] = user.name;
            return userOption;
        });

        let asignees = null;
        if (task.asignees.length > 0) {
            asignees = 
                <div>
                    <ul className="list-group">

                        {task.asignees.map((asignee) => {
                            return (
                                <li className="list-group-item clearfix">
                                    <button className="pull-right close"
                                        onClick={this.removeAsignee.bind(this, asignee.id, this.state.task.id)}
                                    >
                                        <span aria-hidden="true">&times;</span>
                                    </button>

                                    <Avatar email="abc" size="small" cssClass="pull-left" />
                                    {asignee.name}
                                </li>
                            );
                        })}
                    </ul>
                </div>            
        } else {
            asignees = <h4>No asignees.</h4>;
        }

        return (
            <div>
                <Link to={`/project/${task.projectId}/tasks`} className="btn btn-outline btn-primary">
                    <i className="fa fa-list"></i> Task list
                </Link>
                
                <div role="tabpanel" className="mtop-10">
                    <ul className="nav nav-tabs" role="tablist">
                        <li role="presentation" className="active">
                            <a href="#info" aria-controls="info" role="tab" data-toggle="tab">General</a>
                        </li>
                        <li role="presentation">
                            <a href="#asignees" aria-controls="asignees" role="asignees" data-toggle="tab">Asignees</a>
                        </li>
                        <li role="presentation">
                            <a href="#comments" aria-controls="comments" role="comments" data-toggle="tab">Comments</a>
                        </li>
                    </ul>
                    
                    <div className="tab-content">
                        <div role="tabpanel" className="mtop-10 tab-pane active" id="info">
                            <table className="table table-striped">
                                <tbody>
                                    <tr>
                                        <th>Name</th>
                                        <td>{task.name}</td>
                                    </tr>
                                    <tr>
                                        <th>Creator</th>
                                        <td>{task.creator.name}</td>
                                    </tr>
                                    <tr>
                                        <th>End date</th>
                                        <td>{task.endDate}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" className="mtop-10 tab-pane" id="asignees">
                            {asignees}
                            {(() => {
                                if (!users || users.length == 0) {
                                    return ;
                                }
                                return (
                                    <div>
                                        <Input 
                                            type="select"
                                            value={this.state.selectedAsigneeId}
                                            onChange={ (e) => {
                                                this.setState({
                                                    selectedAsigneeId: e.target.value
                                                });
                                            }}
                                            options={users}
                                        />
                                        <button 
                                            onClick={this.addAsignee.bind(this, this.state.selectedAsigneeId, this.state.task.id)}
                                            className="btn btn-block btn-outline btn-primary"
                                        >
                                            <i className="fa fa-plus-circle"></i>{' '}Add asignee
                                        </button>
                                    </div>
                                );
                            })()}
                        </div>
                        <div role="tabpanel" className="mtop-10 tab-pane" id="comments">
                            {(() => {
                                if (this.state.task.comments.length > 0) {
                                    return (
                                        <div className="list-group">
                                            {this.state.task.comments.map((comment) => {
                                                return (
                                                    <div className="comment list-group-item clearfix">
                                                        <div className="col-xs-3">
                                                            <Avatar size="small" email="abd"/>
                                                        </div>
                                                        <div className="col-xs-9">
                                                            <div className="pull-right">
                                                                <a href="#" className="" onClick={this.updateComment.bind(this, comment)}>
                                                                    <i className="fa fa-pencil"></i>
                                                                </a>
                                                                {' '}
                                                                <button 
                                                                    className="close" 
                                                                    onClick={this.removeComment.bind(this, comment)}>
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <ReactMarkdown source={comment.body} />
                                                        </div>
                                                    </div>
                                                );
                                            })}
                                        </div>
                                    );
                                }
                            })()}

                            <Input
                                label="Add your comment"
                                type="textarea"
                                onChange={this.setCommentBody}
                                value={this.state.comment.body}
                                errors={this.formErrors('body')}
                            />
                            <button 
                                className="btn btn-block btn-outline btn-primary mbot-10"
                                onClick={this.saveComment}
                            >
                                <i className="fa fa-plus-circle"></i> Comment
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

});

module.exports = TaskView;