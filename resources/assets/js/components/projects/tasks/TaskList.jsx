import React from 'react';
import { Link } from 'react-router';

const VIEW_MODES = {
    COMPLETE: 1,
    TODO: 2,
};

var TasksList = React.createClass({
    componentWillMount: function () {
        let tasks = $.get('task', {
            projectId: this.state.projectId
        }, (resp) => {
            this.setState({ paginator: resp.paginator});
            this.setState({ tasks: resp.items });
        });
    },
    getInitialState: function () {
        let projectId = parseInt(this.props.params.projectId, 10);

        return {
            viewMode: VIEW_MODES.TODO,
            projectId: projectId,
            paginator: {},
            tasks: []
        };
    },
    setTaskStatus: function (task, isComplete, e) {
        e.preventDefault();

        let tasks = this.state.tasks;
        let index = tasks.indexOf(task);
        let searchedTask = tasks[index];

        searchedTask.complete = isComplete;
        let data = searchedTask;
        data['_method'] = 'PATCH';

        $.post('task/' + task.id, searchedTask, (resp) => {
           this.setState({
               tasks: tasks
           }); 
        });
    },
    changeViewMode: function (viewMode, e) {
        e.preventDefault();

        this.setState({ viewMode: viewMode });
    },
    renderTasks(tasks) {
        
        return tasks.map((task) => {
            let btnClassType = task.complete ? 'btn-warning' : 'btn-primary';
            return (<li className="list-group-item clearfix" key={task.id} 
            >
                <h4 className="task-name">
                    {task.name}
                    <span className="pull-right">
                        <Link to={`/tasks/${task.id}/view`} className="btn btn-success btn-outline">
                            <i className="fa fa-eye"></i>
                        </Link>
                        {' '}
                        <a href="#" 
                            onClick={this.setTaskStatus.bind(this, task, task.complete ? 0 : 1)}
                            className={"btn btn-outline task-switch " + btnClassType}
                            data-toggle="tooltip" data-placement="left" 
                            title={
                                task.complete ? "Reopen" : "Close"
                            }
                        >
                            {(() => {
                                if (!task.complete) {
                                    return <i className="fa fa-check"></i>;
                                } else {
                                    return <i className="fa fa-bell-o"></i>;
                                }
                            })()
                            }
                        </a>
                    </span>
                </h4>
            </li>);
        });
        
    },
    render: function() {
        let tasks = this.state.tasks;
        let viewMode = this.state.viewMode;

        let doneBtnCssClass = viewMode == VIEW_MODES.COMPLETE ? '' : 'btn-outline';
        let undoneBtnCssClass = viewMode == VIEW_MODES.TODO ? '' : 'btn-outline';

        let completeTasks = tasks.filter((t) => t.complete );
        let toDoTasks = tasks.filter((t) => !t.complete );

        return (
            <div>
                <div className="row">
                    <div className="col-sm-2 mbot-10">
                        <Link to={"/project/" + this.state.projectId + "/tasks/create"} className="btn btn-primary btn-outline btn-block">
                            <i className="fa fa-plus-circle"></i> Add
                        </Link>
                    </div>

                    <div className="col-sm-5 mbot-10">
                        <div className="panel panel-info">
                            <div className="panel-heading">
                                <h3 className="panel-title">To Do</h3>
                            </div>
                            <div className="panel-body">
                                {this.renderTasks(toDoTasks)}
                            </div>
                        </div>

                    </div>
                    <div className="col-sm-5 mbot-10">
                        <div className="panel panel-success">
                            <div className="panel-heading">
                                <h3 className="panel-title">Complete</h3>
                            </div>
                            <div className="panel-body">
                                {this.renderTasks(completeTasks)}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = TasksList;