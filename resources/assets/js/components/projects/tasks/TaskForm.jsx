import React from 'react';
import Input from '../../core/Input.jsx';
import FormErrorsMixin from '../../../mixins/FormErrorsMixin';
import FormSubmitMixin from '../../../mixins/FormSubmitMixin';

var TaskForm = React.createClass({
    mixins: [FormErrorsMixin, FormSubmitMixin],

    getInitialState: function () {
        let projectId = this.props.params.projectId;
        
        return {
            task: {
                name: '',
                description: '',
                endDate: '',
                projectId: projectId
            },
            editMode: false
        };
    },
    componentWillMount: function () {
        let taskId = this.props.params.taskId;
        let projectId = this.state.task.projectId;

        if (taskId) {
            $.get('task/' + taskId, function (resp) {
                this.setState({
                    project: response.task,
                    editMode: true
                });
            }.bind(this));
        }
    },
    isEditMode: function () {
        return this.state.editMode;
    },
    setTaskName(e) {
        var task = this.state.task;
        task.name = e.target.value;
        this.setState({ task });
    },
    setTaskDescription(e) {
        var task = this.state.task;
        task.description = e.target.value;
        this.setState({ task });

    },
    setTaskEndDate(date) {
        var task = this.state.task;
        task.endDate = date;
        this.setState({ task });
    },
    saveTask: function(e) {
        let url = 'task';
        let data = this.state.task;

        if (this.isEditMode()) {
            url += '/' + data.id;
            data['_method'] = 'PATCH';
        }

        this.submit(url, data, function (resp) {
            // @TODO show snackbar
        }.bind(this), function (errResp) {
            // @TODO show snackbar
        }.bind(this));
    },

    render: function() {
        return (
            <div className="row">
                <div className="col-sm-12">
                    <Input 
                        label="Name"
                        value={this.state.task.name}
                        onChange={this.setTaskName}
                        errors={this.formErrors('name')}
                    />
                    <Input 
                        label="Description"
                        value={this.state.task.description}
                        onChange={this.setTaskDescription}
                        errors={this.formErrors('description')}
                        type="textarea"
                    />
                    <Input 
                        label="End date"
                        value={this.state.task.endDate}
                        onChange={this.setTaskEndDate}
                        errors={this.formErrors('endDate')}
                        type="date"
                    />

                    <div className="row">
                        <div className="form-group col-sm-2">
                            <button className="btn btn-primary btn-outline btn-block"
                                onClick={this.saveTask}
                            >
                                <i className="fa fa-save"></i> Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

});

module.exports = TaskForm;