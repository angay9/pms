import React from 'react';
import FloatingActionButton from 'material-ui/lib/floating-action-button';
import ContentAdd from 'material-ui/lib/svg-icons/content/add';
import { Link } from 'react-router';
import { browserHistory } from 'react-router';
import ActionDelete from 'material-ui/lib/svg-icons/action/delete';
import ContentCreate from 'material-ui/lib/svg-icons/content/create';
import NavigationChevronRight from 'material-ui/lib/svg-icons/navigation/chevron-right';
import NavigationChevronLeft from 'material-ui/lib/svg-icons/navigation/chevron-left';
import {Table, TableFooter, TableRow, TableHeader, TableBody, TableRowColumn, TableHeaderColumn, FontIcon, IconButton} from 'material-ui';
import DialogMixin from '../../mixins/DialogMixin';

var ClientList = React.createClass({

    mixins: [DialogMixin],

    componentWillMount: function () {
        this.loadData();
    },

    loadData: function (pageNum) {
        let page = pageNum === undefined ? 1 : pageNum;

        $.get('client', {page: page}, function (response) {
            this.setState({
                clients: response.items,
                paginator: {
                    totalCount: response.paginator.total_count,
                    totalPages: response.paginator.total_pages,
                    currentPage: response.paginator.current_page,
                    limit: response.paginator.limit
                }
            });
        }.bind(this));
    },

    getInitialState: function () {
        return {
            clients: [],
            paginator: {}
        };
    },

    deleteItem: function (client, e) {
        e.preventDefault();
        e.stopPropagation();

        this.confirm('', 'Are you sure?', () => {
            $.post('client/' + client.id, {'_method': 'DELETE'}, function (resp) {
                let clients = this.state.clients;
                var index = clients.indexOf(client);
                clients.splice(index, 1);
                this.setState({
                    clients: clients
                });

            }.bind(this));
        });
    },

    render: function() {
        let clients = this.state.clients;
        let currentPage = this.state.paginator.currentPage;
        let totalPages = this.state.paginator.totalPages;
        let totalCount = this.state.paginator.totalCount;

        return (
            <div>
                <Link to="/client/create" className="btn btn-primary btn-outline mbot-10">
                    <i className="fa fa-plus-circle"></i> Add
                </Link>
                <br/>
                <div className="table-responsive">
                    <table className="table table-bordered">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {clients.map(function (client) {
                              return (<tr key={client.id}>
                                <td>{client.id}</td>
                                <td>{client.name}</td>
                                <td>{client.email}</td>
                                <td>
                                    <a href="#" className="btn btn-outline btn-danger" onClick={this.deleteItem.bind(this, client)}>
                                        <i className="fa fa-trash-o"></i>
                                    </a>
                                    {' '}
                                    <Link to={'/client/' + client.id + '/edit'} className="btn btn-success btn-outline">
                                        <i className="fa fa-pencil"></i>
                                    </Link>
                                </td>
                              </tr>);
                            }, this)}
                        </tbody>
                    </table>
                </div>

                <div className="text-center">
                    <ul className="pager">
                        <li className="previous">
                            <button disabled={currentPage === 1} onClick={this.loadData.bind(this, currentPage - 1)}>&larr; Prev</button>
                        </li>
                        <li className="next">
                            <button disabled={currentPage + 1 > totalPages} onClick={this.loadData.bind(this, currentPage + 1)}>Next &rarr;</button>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }

});

export default ClientList;