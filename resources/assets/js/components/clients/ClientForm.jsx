import React from 'react';
import Input from '../core/Input.jsx';
import FormErrorsMixin from '../../mixins/FormErrorsMixin';
import FormSubmitMixin from '../../mixins/FormSubmitMixin';
import Snackbar from 'material-ui/lib/snackbar';

var ClientForm = React.createClass({
    mixins: [FormErrorsMixin, FormSubmitMixin],
    
    componentWillMount: function () {
        let clientId = this.props.params.clientId;

        if (clientId) {
            $.get('client/' + clientId, function (resp) {
                this.setState({
                    client: resp.client,
                    editMode: true
                });
            }.bind(this));
        }
    },

    getInitialState: () => {
        return {
            client: {
                id: '',
                name: '',
                email: '',
                password: ''
            },
            editMode: false,
            snackbar: {
                open: false,
                message: ''
            }
        };
    },

    saveClient: function(e) {
        let url = 'client';
        let data = this.state.client;

        if (this.isEditMode()) {
            url += '/' + data.id;
            data['_method'] = 'PATCH';
        }

        this.submit(url, data, function (resp) {
            // @TODO show snackbar
        }.bind(this), function (errResp) {
            // @TODO show snackbar
        }.bind(this));
    },

    setClientName: function(e) {
        var client = this.state.client;
        client.name = e.target.value;

        this.setState({
            client: client
        });
    },

    setClientEmail: function(e) {
        var client = this.state.client;
        client.email = e.target.value;

        this.setState({
            client: client
        });
    },

    setClientPassword: function(e) {
        var client = this.state.client;
        client.password = e.target.value;

        this.setState({
            client: client
        });
    },

    isEditMode: function () {
        return this.state.editMode;
    },

    render: function() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <Input 
                            value={this.state.client.name} 
                            onChange={this.setClientName} 
                            errors={this.formErrors('name')}
                            label="Name"
                        />
                        { (() => {
                            if (!this.isEditMode()) {
                                <Input 
                                    value={this.state.client.email} 
                                    onChange={this.setClientEmail} 
                                    errors={this.formErrors('email')}
                                    label="Email"
                                />
                            }
                        })()}
                        

                        { (() => {
                            if (!this.isEditMode()) {
                                <Input 
                                    type="password"
                                    value={this.state.client.password}
                                    onChange={this.setClientPassword} 
                                    errors={this.formErrors('password')}
                                    label="Password"
                                />

                            }
                        })() }
                        
                        <div className="form-group">
                            <button onClick={this.saveClient} className="btn btn-primary btn-outline">
                                <i className="fa fa-save"></i> Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = ClientForm;