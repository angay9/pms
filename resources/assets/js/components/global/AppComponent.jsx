import React from 'react';
import AppNavbar from './AppNavbar.jsx';
import Sidebar from './Sidebar.jsx';
import RaisedButton from 'material-ui/lib/raised-button';
import Snackbar from 'material-ui/lib/snackbar';
import App from '../../core/App';
import Alert from './Alert.jsx';
import Confirm from './Confirm.jsx';
import UserTasksChart from '../user/UserTasksChart.jsx';
import UserTasks from '../user/UserTasks.jsx';

var AppComponent = React.createClass({
    getInitialState: function () {
        return {
            alert: {
                open: false,
                message: '',
                type: 'success'
            },
            confirm: {
                open: false,
                message: '',
                title: 'Confirmation',
                yesCallback: new Function (),
                noCallback: new Function (),
                componentHidden: this.clearConfirmProps
            }
        }
    },

    alert(message, type, duration) {
        let alert = this.state.alert;
        let alertType = type || alert.type;
        // let duration = autoHideTime || 3000;

        alert.message = message;
        alert.open = true;
        alert.type = alertType;

        this.setState({
            alert
        });
        if (duration) {
            setTimeout(() => {
                alert.message = '';
                alert.open = false;
                this.setState({
                    alert
                });
            }, duration);   
        }
    },

    confirm(title, message, yesCallback, noCallback) {
        let confirm = this.state.confirm;
        confirm.open = true;
        confirm.title = title ? title : confirm.title;
        confirm.message = message;
        confirm.yesCallback = yesCallback;
        confirm.noCallback = noCallback;

        this.setState({ confirm });
    },
    clearConfirmProps() {
        let confirm = this.state.confirm;
        confirm.open = false;
        confirm.title = this.state.confirm.title;
        confirm.message = '';
        confirm.yesCallback = new Function();
        confirm.noCallback = new Function();

        this.setState({ confirm });
    },

    render: function() {

        return (
            <div className="container">
                <AppNavbar 
                    currentLocation={this.props.location.pathname}
                />

                <div className="container-fluid">
                    <Alert 
                        message={this.state.alert.message} 
                        type={this.state.alert.type} 
                        open={this.state.alert.open}
                        cssClass={'alert-fixed'}
                    />
                    <Confirm 
                        title={this.state.confirm.title}
                        message={this.state.confirm.message}
                        open={this.state.confirm.open}
                        yes={this.state.confirm.yesCallback}
                        no={this.state.confirm.noCallback}
                        componentHidden={this.state.confirm.componentHidden}
                    />
                    {(() => {
                        if (this.props.children) {
                            return (
                                <div className="row">
                                    <div className="col-sm-12">
                                        {
                                            React.cloneElement(this.props.children, {appComponent: this})
                                        }
                                    </div>
                                </div>
                            );   
                        }
                    })()}

                    {(() => {
                        if (this.props.location.pathname == "/") {
                            return (<div className="row">
                                <div className="col-sm-12">
                                    <UserTasksChart appComponent={this} />
                                    <UserTasks appComponent={this} />
                                </div>
                            </div>);

                        }
                    })()}
                </div>
            </div>
        );
    }
});

module.exports = AppComponent;