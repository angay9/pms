import React from 'react';
import {Link} from 'react-router';

var Sidebar = React.createClass({

    render: function () {
        return (
            
            <div className="list-group">
                <Link to={"/project"} className="list-group-item">
                    Projects <span className="label label-primary label-pill pull-right">14</span>
                </Link>
                <Link to={"/client"} className="list-group-item">
                    Clients <span className="label label-primary label-pill pull-right">14</span>
                </Link>
                <Link to={"/profile"} className="list-group-item">
                    Profile
                </Link>
            </div>
        );
    }

});

module.exports = Sidebar;