import React from 'react';

var Confirm = React.createClass({
    propTypes: {
        yes: React.PropTypes.func.isRequired,
        open: React.PropTypes.bool.isRequired,
        no: React.PropTypes.func,
        message: React.PropTypes.string,
        title: React.PropTypes.string,
        componentHidden: React.PropTypes.func
    },
    getDefaultProps() {
        return {
            message: '',
            no: new Function (),
            title: '',
            componentHidden: new Function ()
        };
    },
    getInitialState() {
        return {
            open: this.props.open  
        };
    },
    componentDidMount() {
        if (this.props.open) {
            $(this.refs.confirm).modal('show');   
        }
        $(this.refs.confirm).on('hidden.bs.modal', () => {
            this.props.componentHidden();
        });
    },
    componentDidUpdate(prevProps, prevState) {
        if (this.props.open) {
            $(this.refs.confirm).modal('show');   
        }
    },
    componentWillReceiveProps(nextProps) {
        this.setState({
            open: nextProps.open
        });
    },
    yes() {
        $(this.refs.confirm).modal('hide');
        this.props.yes();
    },
    no() {
        $(this.refs.confirm).modal('hide');
        this.props.no();
    },

    render: function() {
        return (
            <div>
                <div className="modal fade" id="confirm" ref="confirm">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 className="modal-title">{this.props.title}</h4>
                            </div>
                            <div className="modal-body">
                                {this.props.message}
                            </div>
                            <div className="modal-footer">
                                <button type="button" 
                                    className="btn btn-default" 
                                    data-dismiss="modal"
                                    onClick={this.no}
                                >No

                                </button>
                                <button type="button" 
                                    className="btn btn-primary"
                                    onClick={this.yes}
                                >Yes
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

});

module.exports = Confirm;