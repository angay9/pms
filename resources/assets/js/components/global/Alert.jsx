import React from 'react';

var Alert = React.createClass({

    propTypes: {
        open: React.PropTypes.bool.isRequired,
        type: React.PropTypes.oneOf([
            'success', 'danger', 'warning', 'info'
        ]).isRequired,
        message: React.PropTypes.string,
        duration: React.PropTypes.number,
        cssClass: React.PropTypes.string
    },
    getDefaultProps() {
        return {
            cssClass: ''
        };
    },
    componentWillReceiveProps(nextProps) {
        this.setState({
            open: nextProps.open
        });
    },
    getInitialState() {
        
        return {
            open: false
        };
    },
    hide: function () {
        this.setState({open: false});
    },

    render: function() {

        if (!this.state.open || !(this.props.message || this.props.children)) {
            return null;
        }
        let message = this.props.message ? this.props.message : '';

        return (
            <div className={"alert alert-" + this.props.type + ' ' + this.props.cssClass}>
                <button className="close" onClick={this.hide}>
                    <span aria-hidden="true">&times;</span>
                </button>
                {message}
            </div>
        );
    }

});

export default Alert;