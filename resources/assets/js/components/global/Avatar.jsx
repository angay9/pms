import React from 'react';
import gravatar from 'gravatar';

var Avatar = React.createClass({
    propTypes: {
        email: React.PropTypes.string.isRequired,
        size: React.PropTypes.oneOf([
            'small', 'normal'
        ]),
        cssClass: React.PropTypes.string

    },
    render: function() {
        let avatar = null;
        if (this.props.email) {
            let url = gravatar.url('emerleite@gmail.com', {s: '200', r: 'pg', d: '404'});
            avatar = (<img src={url} alt="" className="img-responsive" />);
        } else {
            avatar = (<span className="avatar-text">
                AG
            </span>);
        }
        let className = 'avatar ' + (this.props.size == 'small' ? 'avatar-small ' : ' ') + (this.props.cssClass ? this.props.cssClass : '');
        return (
            <div className={className}>
                {avatar}
            </div>
        );
    }

});

module.exports = Avatar;