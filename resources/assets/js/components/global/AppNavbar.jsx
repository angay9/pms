import React from 'react';
import {Link} from 'react-router';

var AppNavbar = React.createClass({
    propTypes: {
        currentLocation: React.PropTypes.string
    },
    render: function () {
        return (

            <nav className="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="/">PMS</a>
                    </div>
                    <div className="collapse navbar-collapse navbar-ex1-collapse">
                        <ul className="nav navbar-nav">
                            <li className={this.props.currentLocation.indexOf('/project') !== -1 ? "active" : ""}>
                                <Link to={"/project"}>
                                    Projects{' '}
                                </Link>
                            </li>
                            <li className={this.props.currentLocation.indexOf('/client') !== -1 ? "active" : ""}>
                                <Link to={"/client"}>
                                    Clients{' '}
                                </Link>
                            </li>
                        </ul>
                        <ul className="nav navbar-nav navbar-right">
                            <li className="dropdown">
                                <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                    Settings
                                    <span className="caret"></span>
                                </a>
                                <ul className="dropdown-menu">
                                    <li><Link to={"/settings"}>Profile</Link></li>
                                    <li role="separator" className="divider"></li>
                                    <li><a href="/logout">Logout</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                </div>
            </nav>
        );
    }
});

export default AppNavbar;