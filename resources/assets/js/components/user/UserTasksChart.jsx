import React from 'react';
import DialogMixin from '../../mixins/DialogMixin';
import {Bar} from 'react-chartjs';
import moment from 'moment';

var UserTasksChart = React.createClass({
    mixins: [DialogMixin],
    getInitialState() {
        return {
            tasks: [],
            tasksCount: [],
            loaded: false
        };
    },

    componentWillMount() {
        $.get('/task/week', (resp) => {
            this.setState({
                tasks: resp.tasks,
                tasksCount: resp.tasksCount,
                loaded: true
            });
        });
    },

    generateLabels() {
        var curr = new Date();
        var days = [];
        var i = 0;

        while (i < 7) {
            var day = new Date(curr.setDate(curr.getDate() - curr.getDay() + i + 1));
            
            days.push(
                moment(day).format('DD-MM-YYYY')
            );
            i++;
        }
        
        return days;
    },

    generateData() {
        var data = {
            label: "Tasks completed last week",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: this.state.tasksCount
        };

        return data;
    },

    render: function() {
        if (!this.state.loaded) {
            return (
                <div>
                    Loading ...
                </div>
            );
        }

        let data = {
            labels: this.generateLabels(),
            datasets: [this.generateData()]
        };

        return (
            <div>
                <div className="col-md-12">
                    <h3>Your tasks activity this week</h3>
                </div>
                <Bar data={data} options={{
                    responsive: true
                }} width="1200" height="300"/>
            </div>
        );
    }

});

module.exports = UserTasksChart;