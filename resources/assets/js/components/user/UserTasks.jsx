import React from 'react';
import DialogMixin from '../../mixins/DialogMixin';
import {Bar} from 'react-chartjs';
import moment from 'moment';
import { Link } from 'react-router';

var UserTasks = React.createClass({
    mixins: [DialogMixin],
    getInitialState() {
        return {
            tasks: [],
            loaded: false
        };
    },

    componentWillMount() {
        $.get('/user/tasks', (resp) => {
            this.setState({
                tasks: resp.tasks,
                loaded: true
            });
        });
    },

    renderTasksBlock(tasks, type, panelHeading, tasksCount) {
        return (
            <div className={"panel panel-" + type}>
                <div className="panel-heading">
                    <h3 className="panel-title clearfix">
                        {panelHeading}
                        {(() => {
                            if (tasksCount !== undefined) {
                                return (
                                    <span className="label label-success pull-right">
                                        {tasksCount}  
                                    </span>
                                );   
                            }
                        })()}
                    </h3>
                </div>
                <div className="panel-body">
                    <div className="list-group">
                        {tasks.map((t) => {
                            return (
                                <Link key={t.id} to={`/tasks/${t.id}/view`} className="list-group-item" target="_blank">
                                    {t.name}
                                </Link>
                            )
                        })}
                    </div>

                    
                </div>
            </div>
        );
    },

    render: function() {
        if (!this.state.loaded) {
            return (
                <div>
                    Loading ...
                </div>
            );
        }
        let finishedTasks = this.state.tasks.filter((t) => {
            return t.complete;
        });

        let unfinishedTasks = $(this.state.tasks).not(finishedTasks).get();

        return (
            <div>
                <div className="col-md-12">
                    <h3>Your tasks</h3>
                </div>
                <div className="col-sm-6">
                    {this.renderTasksBlock(unfinishedTasks, 'info', 'To Do', unfinishedTasks.length)}    
                </div>
                <div className="col-sm-6">
                    {this.renderTasksBlock(finishedTasks, 'success', 'Complete', finishedTasks.length)}
                </div>      
            </div>
        );
    }

});

module.exports = UserTasks;