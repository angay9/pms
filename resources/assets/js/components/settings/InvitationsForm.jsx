import React from 'react';
import Input from '../core/Input.jsx';
import FormErrorsMixin from '../../mixins/FormErrorsMixin';
import FormSubmitMixin from '../../mixins/FormSubmitMixin';
import DialogMixin from '../../mixins/DialogMixin';

var InvitationsForm = React.createClass({
    mixins: [FormErrorsMixin, FormSubmitMixin, DialogMixin],
    propTypes: {
        containerClass: React.PropTypes.string
    },
    getDefaultProps() {
        return {
            containerClass: ''  
        };
    },
    getInitialState() {
        return {
            email: '',
            role: '',
            roles: null
        };
    },

    componentWillMount() {
        this.loadRoles();
    },
    loadRoles() {
        $.get('role', (resp) => {
            this.setState({
                roles: resp.roles
            });

            // Set default role
            if (resp.roles.length > 0) {
                this.setState({
                    role: resp.roles[0].id
                });
            }
        });
    },
    setRole(e) {
        this.setState({
            role: e.target.value
        });
    },
    setEmail(e) {
        this.setState({
            email: e.target.value
        });
    },

    sendInvitation(e) {
        this.submit('invitation', {
            email: this.state.email,
            role: this.state.role,
        }, () => {
            this.alert('Invitation has been sent.', 'success', 3000);
        });
    },

    render: function() {
        if (!this.state.roles) {
            return (
                <div>
                    Loading...
                </div>
            );
        }
        let roles = this.state.roles.map((r) => {
            let role = {};
            role[r.id] = r.name;

            return role;
        });

        return (
            <div className={"container-fluid " + this.props.containerClass}>
                <div className="row">
                    <div className="col-md-6 col-md-offset-3">
                        <Input 
                            label="User email"
                            value={this.state.email}
                            onChange={this.setEmail}
                            errors={this.formErrors('email')}
                        />
                        <Input 
                            type="select"
                            errors={this.formErrors('role')}
                            label="Role"
                            options={roles}
                            value={this.state.role}
                            onChange={this.setRole}
                        />
                        <button className="btn btn-primary btn-outline"
                            onClick={this.sendInvitation}
                        >
                            <i className="fa fa-btn fa-envelope"></i> Send invitation
                        </button>
                    </div>
                </div>
            </div>
        );
    }

});

module.exports = InvitationsForm;