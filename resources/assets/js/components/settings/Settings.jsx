import React from 'react';
import ProfileSettingsForm from './ProfileSettingsForm.jsx';
import InvitationsForm from './InvitationsForm.jsx';

var Settings = React.createClass({

    render: function() {
        return (
            <div role="tabpanel">
               <ul className="nav nav-tabs" role="tablist">
                    <li role="presentation" className="active">
                        <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Profile</a>
                    </li>
                    <li role="presentation">
                        <a href="#tab" aria-controls="tab" role="tab" data-toggle="tab">Invitations</a>
                    </li>
                </ul>
            
                <div className="tab-content">
                    <div role="tabpanel" className="tab-pane active" id="settings">
                        <ProfileSettingsForm
                            containerClass="mtop-10"
                            appComponent={this.props.appComponent}
                        />
                    </div>
                    <div role="tabpanel" className="tab-pane" id="tab">
                        <InvitationsForm 
                            containerClass="mtop-10"
                            appComponent={this.props.appComponent}
                        />
                    </div>
                </div>
            </div>
        );
    }

});

module.exports = Settings;