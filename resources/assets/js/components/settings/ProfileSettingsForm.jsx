import React from 'react';
import Input from '../core/Input.jsx';
import FormErrorsMixin from '../../mixins/FormErrorsMixin';
import FormSubmitMixin from '../../mixins/FormSubmitMixin';
import DialogMixin from '../../mixins/DialogMixin';

var ProfileSettingsForm = React.createClass({
    mixins: [FormErrorsMixin, FormSubmitMixin, DialogMixin],

    propTypes: {
        containerClass: React.PropTypes.string,
        appComponent: React.PropTypes.object
    },
    getDefaultProps() {
        return {
            containerClass: ''  
        };
    },
    getInitialState() {
        return {
            name: '',
            email: '',
            password: '',
            password_confirmation: '',
        };
    },
    componentWillMount() {
        $.get('profile', (resp) => {
            this.setState({
                name: resp.name,
                email: resp.email,
            });
        });
    },
    changeName(e) {
        this.setState({
            name: e.target.value
        });
    },
    changeEmail(e) {
        this.setState({
            email: e.target.value
        });
    },
    changePassword(e) {
        this.setState({
            password: e.target.value
        });
    },
    changePasswordConfirmation(e) {
        this.setState({
            password_confirmation: e.target.value
        });
    },
    saveProfile() {
        let data = {
            name: this.state.name,
            email: this.state.email
        };

        if (this.state.password) {
            // User wants to change password
            data['password'] = this.state.password;
            data['password_confirmation'] = this.state.password_confirmation;
        }

        this.submit("/profile", data, () => {
            this.alert('Profile settings have been saved.');
        });
    },
    render: function() {
        return (
            <div className={"container-fluid " + this.props.containerClass}>
                <div className="row">
                    <div className="col-md-6 col-md-offset-3">
                        <Input 
                            label="Name"
                            value={this.state.name}
                            onChange={this.changeName}
                            errors={this.formErrors('name')}
                        />

                        <Input 
                            label="Email"
                            value={this.state.email}
                            onChange={this.changeEmail}
                            errors={this.formErrors('email')}
                        />
                        <p className="text-info">
                            {'Leave the new password and the new password confirmation fields empty if you don\'t want to change them'} 
                        </p>

                        <Input 
                            label="New Password"
                            type="password"
                            onChange={this.changePassword}
                            errors={this.formErrors('password')}
                        />

                        <Input 
                            label="New Password Confirmation"
                            type="password"
                            onChange={this.changePasswordConfirmation}
                            errors={this.formErrors('password_confirmation')}
                        />

                        <button className="btn btn-primary btn-outline"
                            onClick={this.saveProfile}
                        >
                            <i className="fa fa-btn fa-save"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        );
    }

});

module.exports = ProfileSettingsForm;