import React from 'react';
import DateTimePicker from 'react-bootstrap-datetimepicker';
import moment from 'moment';

var Input = React.createClass({
    propTypes: {
        name: React.PropTypes.string,
        id: React.PropTypes.string,
        className: React.PropTypes.string,
        value: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.number
        ]),
        errors: React.PropTypes.string,
        onChange: React.PropTypes.func,
        label: React.PropTypes.string,
        type: React.PropTypes.string,
        options: React.PropTypes.array,
        optional: React.PropTypes.bool
    },
    
    render: function() {
        var className = 'form-group' + (this.props.errors ? ' has-error' : '');
        var helpBlock;

        if (this.props.errors) {
            helpBlock = (<div className="help-block">{this.props.errors}</div>);
        }
        return (
            <div className={className}>
                {(() => {
                    if (this.props.label) {
                        return (<label htmlFor="" className="control-label">{this.props.label}</label>); 
                    } 
                }) ()}

                { (() => {
                    if (this.props.type == 'textarea') {
                        return <textarea 
                            name={this.props.name} 
                            id={this.props.id} 
                            cols="30" 
                            rows="5" 
                            className="form-control" 
                            value={this.props.value}
                            onChange={this.props.onChange}
                        ></textarea> 
                    } else if (this.props.type == 'date' || this.props.type == 'datetime') {
                        return (
                            <DateTimePicker
                                dateTime={this.props.value ? this.props.value : moment().format('YYYY-MM-DD')}
                                format="YYYY-MM-DD"
                                inputFormat="YYYY-MM-DD"
                                mode={this.props.type}
                                onChange={this.props.onChange}
                            />
                        );
                    } else if (this.props.type == 'select') {
                        return (
                            <select 
                                type={this.props.type ? this.props.type : 'text'} 
                                onChange={this.props.onChange} 
                                name={this.props.name} 
                                id={this.props.id}
                                className="form-control"
                                value={this.props.value}
                            >
                                {(() => {
                                    if (this.props.optional) {
                                        return (<option></option>);
                                    }
                                }) ()}

                                {this.props.options.map((option) => {
                                    var value = Object.keys(option)[0];
                                    return (
                                        <option value={value} key={value}
                                        >
                                            {option[value]}
                                        </option>
                                    )
                                })}
                            </select>
                        );
                    }

                    return <input type={this.props.type ? this.props.type : 'text'} onChange={this.props.onChange} name={this.props.name} id={this.props.id} className="form-control" value={this.props.value} />
                }) ()}
                {helpBlock}
            </div>
        );
    }

});

module.exports = Input;