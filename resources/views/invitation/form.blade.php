@extends('layouts.auth')
@section('content')
    <div class="container mtop-10">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Invitation Activation</div>
                    <div class="panel-body">
                        <p class="text-center text-info">
                            In order to complete the invitation activation process
                            please fill in your personal details.
                        </p>
                        <form method="POST" class="form-horizontal" action="{{ route('invitation.activate', compact('code')) }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            @foreach ($errors->get('name') as $error)
                                                <strong>{{ $error }}</strong>
                                            @endforeach
                                        </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            @foreach ($errors->get('password') as $error)
                                                <strong>{{ $error }}</strong>
                                            @endforeach
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Password Confirmation</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            @foreach ($errors->get('password_confirmation') as $error)
                                                <strong>{{ $error }}</strong>
                                            @endforeach
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button class="btn btn-primary">
                                        <i class="fa fa-btn fa-check-circle"></i> Activate
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop