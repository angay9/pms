@extends('layouts.email')
@section('content')
    <h3>Welcome to our Project Management System</h3>
    <div>
        <p>
            In order to activate your account please visit this link:
            {{ route('invitation.showInvitationForm', ['code' => $invitationLink]) }}
        </p>    
    </div>
@stop