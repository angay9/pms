<?php

namespace App\Jobs\Asignees;

use App\Jobs\Job;
use App\Models\Task;
use App\Repositories\TaskRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RemoveAsignee extends Job
{
    use InteractsWithQueue, SerializesModels;
    
    /**
     * Task Repository
     *
     * @var TaskRepository
     */
    private $taskRepo;

    private $taskId;

    private $asigneeId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TaskRepository $taskRepo, $taskId, $asigneeId)
    {
        $this->taskRepo = $taskRepo;
        $this->taskId = $taskId;
        $this->asigneeId = $asigneeId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this
            ->taskRepo
            ->removeParticipant($this->taskId, $this->asigneeId)
        ;
    }
}
