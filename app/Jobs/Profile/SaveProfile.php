<?php

namespace App\Jobs\Profile;

use App\Jobs\Job;
use App\Repositories\UserRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SaveProfile extends Job
{
    use InteractsWithQueue, SerializesModels;
    
    protected $userRepo;
    
    protected $name;
    
    protected $email;
    
    protected $password;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepo, $name, $email, $password = null)
    {
        $this->userRepo = $userRepo;
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userData = [
            'name'  =>  $this->name,
            'email' =>  $this->email
        ];
        if ($this->password) {
            $userData['password'] = bcrypt($this->password);
        }
        $this->userRepo->update($userData, auth()->id());
    }
}
