<?php

namespace App\Jobs\Comments;

use App\Jobs\Job;
use App\Repositories\CommentRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateComment extends Job
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Comment Repository
     *
     * @var \App\Repositories\CommentRepository
     */
    private $commentRepsitory;
    
    /**
     * Comment's body
     * 
     * @var string
     */
    private $body;

    private $commentableId;

    private $commentableType;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CommentRepository $comemntRepository, $body, $commentableId, $commentableType)
    {
        $this->comemntRepository = $comemntRepository;
        $this->body = $body;
        $this->commentableId = $commentableId;
        $this->commentableType = $commentableType;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        return $this->comemntRepository->create([
            'body'  =>  $this->body,
            'commentable_id'    =>  $this->commentableId,
            'commentable_type'  =>  $this->commentableType
        ]);
    }
}
