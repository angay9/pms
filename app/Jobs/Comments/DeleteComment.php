<?php

namespace App\Jobs\Comments;

use App\Jobs\Job;
use App\Repositories\CommentRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteComment extends Job
{
    use InteractsWithQueue, SerializesModels;

    /**
     * User Repository
     *
     * @var \App\Repositories\CommentRepository
     */
    protected $commentRepository;

    /**
     * Project id
     * 
     * @var int
     */
    protected $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CommentRepository $commentRepository, $id)
    {
        $this->commentRepository = $commentRepository;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->commentRepository->delete($this->id);
    }
}
