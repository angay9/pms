<?php

namespace App\Jobs\Comments;

use App\Jobs\Job;
use App\Repositories\CommentRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateComment extends Job
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Comment Repository
     *
     * @var \App\Repositories\CommentRepository
     */
    private $commentRepsitory;
    
    private $id;

    /**
     * Comment's body
     * 
     * @var string
     */
    private $body;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CommentRepository $commentRepository, $id, $body)
    {
        $this->commentRepository = $commentRepository;
        $this->id = $id;
        $this->body = $body;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->commentRepository->update([
            'body'  =>  $this->body,
        ], $this->id);
    }
}
