<?php

namespace App\Jobs\Tasks;

use App\Jobs\Job;
use App\Repositories\TaskRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateTask extends Job
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Task id
     * 
     * @var int
     */
    private $id;

    /**
     * Project Repository
     *
     * @var \App\Repositories\TaskRepository
     */
    private $taskRepository;
    
    /**
     * Project's name
     * 
     * @var string
     */
    private $name;

    /**
     * Project's description
     * 
     * @var string
     */
    private $description;
    
    /**
     * End date
     * 
     * @var string
     */
    private $endDate;

    /**
     * Project Id
     * 
     * @var int
     */
    private $projectId;

    /**
     * Complete
     * 
     * @var boolean
     */
    private $complete;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TaskRepository $taskRepository, $id, $name, $description, $projectId, $endDate, $complete = false)
    {
        $this->taskRepository = $taskRepository;
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->projectId = $projectId;
        $this->endDate = $endDate;
        $this->complete = (bool)$complete;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->taskRepository->update([
            'name'  =>  $this->name,
            'description'   =>  $this->description,
            'project_id'  => $this->projectId,
            'end_date'   =>  date('Y-m-d', strtotime($this->endDate)),
            'complete'  =>  $this->complete
        ], $this->id);
    }
}
