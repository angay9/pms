<?php

namespace App\Jobs\Tasks;

use App\Jobs\Job;
use App\Repositories\TaskRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateTask extends Job
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Project Repository
     *
     * @var \App\Repositories\TaskRepository
     */
    private $taskRepository;
    
    /**
     * Project's name
     * 
     * @var string
     */
    private $name;

    /**
     * Project's description
     * 
     * @var string
     */
    private $description;
    
    /**
     * End date
     * 
     * @var string
     */
    private $endDate;

    /**
     * Project Id
     * 
     * @var int
     */
    private $projectId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TaskRepository $taskRepository, $name, $description, $projectId, $endDate)
    {
        $this->taskRepository = $taskRepository;
        $this->name = $name;
        $this->description = $description;
        $this->projectId = $projectId;
        $this->endDate = $endDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   
        $this->taskRepository->create([
            'name'  =>  $this->name,
            'description'   =>  $this->description,
            'project_id'  => $this->projectId,
            'end_date'   =>  date('Y-m-d', strtotime($this->endDate)),
            'creator_id'    =>  auth()->user()->id
        ]);
    }
}
