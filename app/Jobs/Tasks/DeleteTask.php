<?php

namespace App\Jobs\Tasks;

use App\Jobs\Job;
use App\Repositories\TaskRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteTask extends Job
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Task Repository
     *
     * @var \App\Repositories\TaskRepository
     */
    protected $taskRepository;

    /**
     * Project id
     * 
     * @var int
     */
    protected $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TaskRepository $taskRepository, $id)
    {
        $this->taskRepository = $taskRepository;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->taskRepository->delete($this->id);
    }
}
