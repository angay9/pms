<?php

namespace App\Jobs\Invitations;

use App\Exceptions\Invitations\InvitationNotFoundException;
use App\Jobs\Job;
use App\Models\Role;
use App\Repositories\InvitationRepository;
use App\Repositories\UserRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use DB;

class ActivateInvitation extends Job
{

    use InteractsWithQueue, SerializesModels;

    protected $invitationRepo;
    
    protected $code;

    protected $name;
    
    protected $password;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(InvitationRepository $invitationRepo, UserRepository $userRepo, $code, $name, $password)
    {
        $this->invitationRepo = $invitationRepo;
        $this->userRepo = $userRepo;
        $this->code = $code;
        $this->name = $name;
        $this->password = $password;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $invitation = $this->invitationRepo->findBy('code', $this->code);
        
        if (!$invitation) {
            throw new InvitationNotFoundException;
        }
        
        DB::transaction(function () use ($invitation) {
            $this->userRepo->createWithRole([
                'email' =>  $invitation->email,
                'name'  => $this->name, // Change this
                'password'  =>  bcrypt($this->password), // Change this
            ], $invitation->role);

            $this->invitationRepo->delete($invitation->id);
        });
    }
}
