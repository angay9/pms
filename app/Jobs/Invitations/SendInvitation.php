<?php

namespace App\Jobs\Invitations;

use App\Jobs\Job;
use App\Services\Mail\AppMailerInterface;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repositories\InvitationRepository;

class SendInvitation extends Job
{
    use InteractsWithQueue, SerializesModels;

    protected $mailer;

    protected $emailReceiver;

    protected $role;

    protected $invitationRepo;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(AppMailerInterface $mailer, InvitationRepository $invitationRepo, $emailReceiver, $role)
    {
        $this->mailer = $mailer;
        $this->invitationRepo = $invitationRepo;
        $this->emailReceiver = $emailReceiver;
        $this->role = $role;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $invitationCode = sha1(time() . auth()->id());

        $this->mailer->sendInvitationEmail(
            $this->emailReceiver, $invitationCode
        );

        $invitation = $this->invitationRepo->create([
            'email' =>  $this->emailReceiver,
            'code'  =>  $invitationCode,
            'role_id'   =>  $this->role
        ]);

    }
}
