<?php

namespace App\Jobs\Users;

use App\Jobs\Job;
use App\Repositories\UserRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteClient extends Job
{
    use InteractsWithQueue, SerializesModels;

    /**
     * User Repository
     *
     * @var \App\Repositories\UserRepository
     */
    protected $userRepository;

    /**
     * Client id
     * 
     * @var int
     */
    protected $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository, $id)
    {
        $this->userRepository = $userRepository;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->userRepository->delete($this->id);
    }
}
