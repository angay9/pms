<?php

namespace App\Jobs\Users;

use App\Jobs\Job;
use App\Models\Role;
use App\Repositories\UserRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateClient extends Job
{
    use InteractsWithQueue, SerializesModels;

    /**
     * User Repository
     *
     * @var \App\Repositories\UserRepository
     */
    private $userRepository;
    
    /**
     * Client name
     * 
     * @var string
     */
    private $name;

    /**
     * Client email
     * 
     * @var string
     */
    private $email;
    
    /**
     * Client password
     * 
     * @var string
     */
    private $password;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository, $name, $email, $password)
    {
        $this->userRepository = $userRepository;
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->userRepository->createWithRole([
            'name'  =>  $this->name,
            'email' =>  $this->email,
            'password'  =>  bcrypt($this->password)
        ], Role::ROLE_CLIENT);
    }
}
