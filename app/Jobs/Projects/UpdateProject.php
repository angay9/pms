<?php

namespace App\Jobs\Projects;

use App\Jobs\Job;
use App\Repositories\ProjectRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateProject extends Job
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Project Repository
     *
     * @var \App\Repositories\ProjectRepository
     */
    private $projectRepository;
    
    /**
     * Project id
     * 
     * @var int
     */
    private $id;

    /**
     * Project's name
     * 
     * @var string
     */
    private $name;

    /**
     * Project's description
     * 
     * @var string
     */
    private $description;
    
    /**
     * Project's client id
     * 
     * @var string
     */
    private $endDate;

    /**
     * Project client id
     * 
     * @var int
     */
    private $clientId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ProjectRepository $projectRepository, $id, $name, $description, $clientId, $endDate)
    {
        $this->projectRepository = $projectRepository;
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->clientId = $clientId;
        $this->endDate = $endDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->projectRepository->update([
            'name'  =>  $this->name,
            'description'   =>  $this->description,
            'client_id'  => $this->clientId,
            'end_date'   =>  date('Y-m-d', strtotime($this->endDate)) 
        ], $this->id);
    }
}
