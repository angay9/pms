<?php

namespace App\Jobs\Projects;

use App\Jobs\Job;
use App\Repositories\ProjectRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteProject extends Job
{
    use InteractsWithQueue, SerializesModels;

    /**
     * User Repository
     *
     * @var \App\Repositories\ProjectRepository
     */
    protected $projectRepository;

    /**
     * Project id
     * 
     * @var int
     */
    protected $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ProjectRepository $projectRepository, $id)
    {
        $this->projectRepository = $projectRepository;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->projectRepository->delete($this->id);
    }
}
