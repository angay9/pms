<?php

namespace App\Providers;

use App\Services\Mail\AppMailerInterface;
use App\Services\Mail\AppMailer;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AppMailerInterface::class, AppMailer::class);
        \DB::enableQueryLog();
    }
}
