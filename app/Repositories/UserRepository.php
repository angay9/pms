<?php

namespace App\Repositories;

use App\Models\User;
use Bosnadev\Repositories\Eloquent\Repository;

class UserRepository extends Repository 
{
    /**
     * Get model class
     * 
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Create user with role
     * 
     * @param  array  $data
     * @param  string $role
     * @return void
     */
    public function createWithRole(array $data, $role)
    {
        $user = $this->create($data);

        $user->assignRole($role);
    }
}