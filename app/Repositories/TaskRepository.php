<?php

namespace App\Repositories;

use App\Models\Task;
use Bosnadev\Repositories\Eloquent\Repository;
use Carbon\Carbon;

class TaskRepository extends Repository 
{
    /**
     * Get model class
     * 
     * @return string
     */
    public function model()
    {
        return Task::class;
    }

    public function paginateTasks($projectId = null, $perPage = 1, array $columns = ['*'], array $relations = [])
    {
        if ($projectId) {
            return Task::where('project_id', $projectId)->paginate($perPage, $columns);
        }

        return Task::paginate($perPage, $columns);
    }

    public function userTasks(array $relations = [], array $columns = ['*'])
    {
        return auth()
            ->user()
            ->tasks()
            ->with(array_merge(['participants'], $relations))
            ->where('participant_role', Task::TASK_PARTICIPANT_ASIGNEE)
            ->get()
        ;
    }

    /**
     * Add asignee
     *
     * @param int $taskId
     * @param int $usrId
     * @param int $role
     * 
     * @return void
     */
    public function addParticipant($taskId, $userId, $role)
    {
        $task = $this->model->findOrFail($taskId);

        $task->participants()->attach($userId, ['participant_role' => $role]);
    }

    /**
     * Remove asignee
     *
     * @param int $taskId
     * @param int $usrId
     * @param int $role
     * 
     * @return void
     */
    public function removeParticipant($taskId, $userId)
    {
        $task = $this->model->findOrFail($taskId);
        
        $task
            ->participants()
            ->detach($userId)
        ;
    }

    /**
     * Find task with related models
     * 
     * @param  int   $id
     * @param  array  $with
     * @return Task
     */
    public function findWith($id, array $with = [])
    {
        return Task::with($with)->findOrFail($id);
    }

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute="id") 
    {
        $model = $this->model->where($attribute, '=', $id)->firstOrFail();

        return $model->update($data);
    }

    public function userTasksInRange($start, $end)
    {

        // dd($endOfWeek);
        $tasks = auth()->user()->tasks()
                ->with('participants')
                ->where('participant_role', Task::TASK_PARTICIPANT_ASIGNEE)
                // ->whereBetween('closed_at', [])
                ->where('closed_at', '>=', $start)
                ->where('closed_at', '<', $end)
                ->where('complete', true)
                ->get()
            ;
        
        return $tasks;
    }
}