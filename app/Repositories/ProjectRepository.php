<?php

namespace App\Repositories;

use App\Models\Project;
use Bosnadev\Repositories\Eloquent\Repository;

class ProjectRepository extends Repository
{
    /**
     * Get model class
     * 
     * @return string
     */
    public function model()
    {
        return Project::class;
    }

    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 1, $columns = array('*'), array $relations = array()) 
    {
        $this->applyCriteria();
        
        return Project::with($relations)->paginate($perPage, $columns);
    }

}