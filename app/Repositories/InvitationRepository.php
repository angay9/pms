<?php

namespace App\Repositories;

use App\Models\Invitation;
use Bosnadev\Repositories\Eloquent\Repository;

class InvitationRepository extends Repository 
{
    public function model()
    {
        return Invitation::class;
    }

    public function findOneByCode($code)
    {
        return $this->model->where('code', $code)->first();
    }
}