<?php

namespace App\Repositories;

use App\Models\Comment;
use Bosnadev\Repositories\Eloquent\Repository;

class CommentRepository extends Repository
{
    /**
     * Get model class
     * 
     * @return string
     */
    public function model()
    {
        return Comment::class;
    }

    /**
     * Find comments for Commentable
     * 
     * @param  int $commentableId
     * @param  string $commentableType
     * @return
     */
    public function findComments($commentableId, $commentableType)
    {
        return Comment::where('commentable_id', (int)$commentableId)
            ->where('commentable_type', (int)$commentableType)
            ->get()
        ;
    }
}