<?php

namespace App\Repositories;

use App\Models\Role;
use App\Models\User;
use App\Models\TaskParticipant;
use Bosnadev\Repositories\Eloquent\Repository;

class RoleRepository extends Repository 
{
    public function model()
    {
        return Role::class;
    }

    /**
     * Get users by role
     * 
     * @param  string $role
     * @param  int    $count
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function paginateByRole($role, $count = 20)
    {
        return $this
            ->model
            ->whereName($role)
            ->firstOrFail()
            ->users()
            ->paginate($count)
        ;
    }

    /**
     * Find all users by role
     * 
     * @param  string $role Role
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findUsersByRole($role, $taskId = null)
    {
        if ($taskId) {
            $participantsIds = TaskParticipant::whereTaskId($taskId)->lists('user_id');
            return $this
                    ->model
                    ->whereName($role)
                    ->firstOrFail()
                    ->users()
                    ->whereNotIn('id', $participantsIds)
                    ->get()
                ;
        }

        return $this
            ->model
            ->whereName($role)
            ->firstOrFail()
            ->users()
            ->get()
        ;
    }
}