<?php

namespace App\Models;

use App\Models\Behaviours\HasRoles;
use App\Models\Task;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tasks()
    {
        return $this->belongsToMany(Task::class, 'task_participants');
    }

    public function createdTasks()
    {
        return $this->hasMany(Task::class, 'creator_id', 'id');
    }
}
