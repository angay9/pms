<?php

namespace App\Models;

use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['name', 'description', 'client_id', 'end_date'];

    public function client()
    {
        return $this->belongsTo(User::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function getCompleteTasksCount()
    {
        return $this->tasks()->where('complete', 1)->count();
    }

    public function getIncompleteTasksCount()
    {
        return $this->tasks()->where('complete', 0)->count();
    }
}
