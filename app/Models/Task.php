<?php

namespace App\Models;

use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    const TASK_PARTICIPANT_ASIGNEE = 1;
    const TASK_PARTICIPANT_WATCHER = 2;

    protected $fillable = ['name', 'description', 'project_id', 'end_date', 'complete', 'creator_id']; 

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function participants()
    {
        return $this->belongsToMany(User::class, 'task_participants');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function asignees()
    {
        return $this->participants()->where('participant_role', static::TASK_PARTICIPANT_ASIGNEE);
    }

    public function watchers()
    {
        return $this->participants()->where('participant_role', static::TASK_PARTICIPANT_WATCHER);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public static function boot()
    {
       parent::boot();

       static::updating(function($task)
       {
            if ($task->complete) {
                $task->closed_at = date('Y-m-d');
            } else {
                $task->closed_at = null;
            }
       });
   }
}
