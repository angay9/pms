<?php

namespace App\Models;

use App\Models\Role;
use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    protected $table = 'invitations';

    protected $fillable = ['email', 'code', 'role_id'];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
