<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class SendInvitationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' =>  'required|max:255|email|unique:invitations',
            'role'  =>  'exists:roles,id'
        ];
    }

    public function messages()
    {
        return [
            'email.unique'  =>  'You cannot send invitation to this user because he already has an account or invitation was already sent.'
        ];
    }
}
