<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SaveProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required|max:255',
            'email' =>  'required|unique:users,email,' . auth()->id(),
            'password'  =>  'min:6|max:255|confirmed',
            'password_confirmation' =>  'min:6|max:255',
        ];
    }
}
