<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateProjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'    =>  'required|exists:projects,id',
            'name'  =>  'required|max:255',
            'description'    =>  'required|max:255',
            'endDate'  =>  'required|date|after:now',
            'clientId'  =>  'required|exists:users,id'
        ];
    }
}
