<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web'/*, 'auth'*/], 'prefix' => 'app'], function () {
    // Clients
    Route::resource('/client', 'ClientController');

    // Projects
    Route::resource('/project', 'ProjectController');
    
    Route::resource('/role', 'RoleController');

    // Tasks
    Route::get('/user/tasks', 'TaskController@userTasks');
    Route::get('/task/week', 'TaskController@userTasksThisWeek');
    Route::resource('/task', 'TaskController');
    Route::post('/task/{task}/comment/add', 'TaskController@addComment');
    Route::put('/task/{task}/comment/{comment}/update', 'TaskController@updateComment');
    Route::delete('/task/{task}/comment/{comment}/remove', 'TaskController@removeComment');

    // Comments
    // Route::resource('/comment', 'CommentController');

    // Asignees
    Route::get('/asignee', 'AsigneeController@index');
    Route::post('/asignee/add/{task}/{asignee}', 'AsigneeController@store');
    Route::delete('/asignee/remove/{task}/{asignee}', 'AsigneeController@destroy');

    // Invitations
    Route::post('/invitation', 'InvitationController@sendInvitation');
    Route::get('/invitation/{code}', 'InvitationController@showInvitationForm')->name('invitation.showInvitationForm');
    Route::post('/invitation/activate/{code}', 'InvitationController@activateInvitation')->name('invitation.activate');

    // Profile
    Route::get('/profile', 'ProfileController@getProfileSettings');
    Route::post('/profile', 'ProfileController@saveProfile');
});

Route::group(['middleware' => 'web'], function () {

    Route::auth();

    // Redirect to index so that react.js can handle routing
    // This route has to be below the other ones, so that it 
    // doesn't override them
    Route::any('{path?}', function() {
        return view("index");
    })->where("path", ".+")->middleware(['auth']);
});

