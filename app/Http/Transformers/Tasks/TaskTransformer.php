<?php

namespace App\Http\Transformers\Tasks;

use App\Http\Transformers\Transformer;
use App\Http\Transformers\Users\UserTransformer;

class TaskTransformer extends Transformer
{
    public function transform($task)
    {
        $userTransfor = app(UserTransformer::class);
        
        return [
            'id'    =>  $task->id,
            'name' =>  $task->name,
            'description'   =>  $task->description,
            'projectId' =>  (int)$task->project_id,
            'creatorId' =>  (int)$task->creator_id,
            'endDate'   =>  $task->end_date,
            'complete'  =>  (int)$task->complete,
            'closedAt' => $task->closed_at,
            'creator'   =>  $task->creator ? $userTransfor->transform($task->creator) : null,
            'asignees'  =>  $userTransfor->transformCollection($task->asignees->all()),
            'comments'  =>  $task->relationLoaded('comments') ? $task->comments->toArray() : []
        ];
    }
}