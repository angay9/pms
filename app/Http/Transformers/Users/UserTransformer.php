<?php

namespace App\Http\Transformers\Users;

use App\Http\Transformers\Transformer;

class UserTransformer extends Transformer 
{
    /**
     * Transform user into array
     * 
     * @param  \App\Models\User $user
     * @return array
     */
    public function transform($user)
    {
        return [
            'id'    =>  $user->id,
            'name'    =>  $user->name,
            'email'    =>  $user->email,
        ];
    }
}