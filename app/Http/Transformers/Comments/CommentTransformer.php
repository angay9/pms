<?php

namespace App\Http\Transformers\Comments;

use App\Http\Transformers\Transformer;

class CommentTransformer extends Transformer
{
    public function transform($comment)
    {
        return [
            'id'    =>  $comment->id,
            'body'  =>  $comment->body,
            'commentableId' => $comment->commentable_id,
            'commentableType' => $comment->commentable_type,
        ];
    }
}