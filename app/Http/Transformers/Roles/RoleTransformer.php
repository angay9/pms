<?php

namespace App\Http\Transformers\Roles;

use App\Http\Transformers\Transformer;

class RoleTransformer extends Transformer 
{
    public function transform($item)
    {
        return [
            'id'    =>  $item->id,
            'name'  =>  $item->name,
            'label' =>  $item->label
        ];
    }
}