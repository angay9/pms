<?php

namespace App\Http\Transformers\Projects;

use App\Http\Transformers\Transformer;
use App\Http\Transformers\Users\UserTransformer;

class ProjectTransformer extends Transformer
{
    public function transform($project)
    {
        $userTransformer = app(UserTransformer::class);

        return [
            'id'    =>  $project->id,
            'name'  =>  $project->name,
            'description'   =>  $project->description,
            'endDate'  =>  $project->end_date,
            'clientId'  =>  (int)$project->client_id,
            'client'    =>  $project->client ? $userTransformer->transform($project->client) : null,
            'tasksCount' =>  $project->tasks()->count(),
            'completeTasksCount' =>  $project->getCompleteTasksCount(),
            'incompleteTasksCount' =>  $project->getIncompleteTasksCount(),
        ];
    }
}