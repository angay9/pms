<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CreateClientRequest;
use App\Http\Requests\UpdateClientRequest;
use App\Http\Transformers\Users\UserTransformer;
use App\Jobs\Users\CreateClient;
use App\Jobs\Users\DeleteClient;
use App\Jobs\Users\UpdateClient;
use App\Models\Role;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Role repository
     * 
     * @var \App\Repositories\RoleRepository
     */
    private $roleRepo;

    /**
     * User repository
     * 
     * @var \App\Repositories\UserRepository
     */
    private $userRepo;

    /**
     * User Transformer
     *
     * @var \App\Http\Transformers\Users\UserTransformer
     */
    private $userTransformer;

    /**
     * Constructor
     * 
     * @param \App\Repositories\RoleRepository $roleRepo
     * @param \App\Repositories\UserRepository $userRepo
     */
    public function __construct(RoleRepository $roleRepo, UserRepository $userRepo, UserTransformer $userTransformer)
    {
        $this->roleRepo = $roleRepo;
        $this->userRepo = $userRepo;
        $this->userTransformer = $userTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = $this->roleRepo->paginateByRole(Role::ROLE_CLIENT, 10);

        return $this->respondWithPagination(
            $clients, $this->userTransformer->transformCollection(
                $clients->items()
            )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CreateClientRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateClientRequest $request)
    {
        $this->dispatch(
            app(CreateClient::class, $request->only('name', 'email', 'password'))
        );

        return $this->respondCreated('Client has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $client)
    {
        return $this->respond([
            'client'    =>  $this->userTransformer->transform($client)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateClientRequest  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClientRequest $request, $id)
    {
        $this->dispatch(
            app(UpdateClient::class, ['id' => $id, 'name' => $request->input('name')])
        );

        return $this->respondSuccess();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->dispatch(
            app(DeleteClient::class, ['id' => $id])
        );
        return $this->respondSuccess();
    }
}
