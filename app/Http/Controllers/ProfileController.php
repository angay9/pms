<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\SaveProfileRequest;
use App\Jobs\Profile\SaveProfile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function getProfileSettings()
    {
        $user = auth()->user();
        
        return $this->respond([
            'name'  =>  $user->name,
            'email' =>  $user->email,
        ]);
    }

    public function saveProfile(SaveProfileRequest $request)
    {
        $job = app(SaveProfile::class, $request->only('name', 'email', 'password'));
        
        $this->dispatch($job);

        return $this->respondSuccess();
    }
}
