<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Transformers\Users\UserTransformer;
use App\Jobs\Asignees\AddAsignee;
use App\Jobs\Asignees\RemoveAsignee;
use App\Models\Role;
use App\Models\Task;
use App\Models\User;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;

class AsigneeController extends Controller
{
    /**
     * User transformer
     * 
     * @var UserTransformer
     */
    protected $userTransformer;
    
    /**
     * Role repository
     * 
     * @var RoleRepository
     */
    protected $roleRepo;

    public function __construct(UserTransformer $userTransformer, RoleRepository $roleRepo)
    {
        $this->userTransformer = $userTransformer;
        $this->roleRepo = $roleRepo;
    }


    /**
     * Return a list of all users that can be
     * assigned to a project
     * 
     * @return
     */
    public function index(Request $request)
    {
        $taskId = $request->get('taskId');
        $users = $this->roleRepo->findUsersByRole(Role::ROLE_USER, $taskId);

        return $this->respond(
            $this->userTransformer->transformCollection(
                $users->all()
            )
        );
    }

    /**
     * @param int $task
     * @param int $asignee
     * 
     * @return \Illuminate\Http\Response
     */
    public function store($task, $asignee)
    {
        $this->dispatch(
            app(AddAsignee::class, ['taskId' => (int)$task, 'asigneeId' => (int)$asignee])
        );

        return $this->respondSuccess();
    }

    /**
     * @param int $taskId
     * @param int $asigneeId
     * 
     * @return \Illuminate\Http\Response
     */
    public function destroy($task, $asignee)
    {

        $this->dispatch(
            app(RemoveAsignee::class, ['taskId' => (int)$task, 'asigneeId' => (int)$asignee])
        );

        return $this->respondSuccess();
    }
}
