<?php

namespace App\Http\Controllers\Behaviours;

use App\Http\Requests\CreateCommentRequest;
use App\Http\Requests\DeleteCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Http\Transformers\Comments\CommentTransformer;
use App\Jobs\Comments\CreateComment;
use App\Jobs\Comments\UpdateComment;
use App\Jobs\Comments\DeleteComment;

trait HandlesCommentsRequests
{
    protected function performCommentCreate(CreateCommentRequest $request, $commentableId, $commentableType)
    {
        $comment = $this->dispatch(
            app(CreateComment::class, [
                'body'  =>  $request->get('body'),
                'commentableId' =>  $commentableId,
                'commentableType'   =>  $commentableType
            ])
        );
        $commentTranformer = app(CommentTransformer::class);

        return $this->respond([
            'comment'   =>  $commentTranformer->transform($comment)
        ]);
    }

    protected function performCommentDelete($commentId)
    {
        $this->dispatch(
            app(DeleteComment::class, ['id' => $commentId])
        );

        return $this->respondSuccess();
    }

    protected function performCommentUpdate(UpdateCommentRequest $request, $commentId)
    {
        $this->dispatch(
            app(UpdateComment::class, [
                'id'    =>  $commentId,
                'body'  =>  $request->get('body'),
            ])
        );

        return $this->respondSuccess();
    }
}