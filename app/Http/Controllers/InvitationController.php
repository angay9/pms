<?php

namespace App\Http\Controllers;

use App\Exceptions\Invitations\InvitationNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\ActivateInvitationRequest;
use App\Http\Requests\SendInvitationRequest;
use App\Jobs\Invitations\ActivateInvitation;
use App\Jobs\Invitations\SendInvitation;
use App\Repositories\InvitationRepository;
use Illuminate\Http\Request;

class InvitationController extends Controller
{
    public function sendInvitation(SendInvitationRequest $request)
    {
        $job = app(SendInvitation::class, [
            'emailReceiver' =>  $request->get('email'),
            'role'  =>  $request->get('role')
        ]);

        $this->dispatch($job);

        return $this->respondSuccess();
    }

    public function showInvitationForm(InvitationRepository $repo, $code)
    {
        $invitation = $repo->findOneByCode($code);

        if (!$invitation) {
            abort(404);
        }

        return view('invitation.form', ['code' => $code]);
    }

    public function activateInvitation(ActivateInvitationRequest $request, InvitationRepository $invitationRepo, $code)
    {
        $job = app(ActivateInvitation::class, array_merge(
                ['code' => $code], 
                $request->only(['name', 'password'])
            )
        );

        try {
            $this->dispatch($job);

            return redirect('/');
        } catch (InvitationNotFoundException $e) {
            abort(404);
        }
    }
}
