<?php

namespace App\Http\Controllers;

use App\Http\Transformers\Roles\RoleTransformer;
use App\Repositories\RoleRepository;

class RoleController extends Controller 
{
    private $roleRepo;

    private $roleTransformer;

    public function __construct(RoleRepository $roleRepo, RoleTransformer $roleTransformer)
    {
        $this->roleRepo = $roleRepo;
        $this->roleTransformer = $roleTransformer;
    }

    public function index()
    {
        $roles = $this->roleRepo->all();

        return $this->respond([
            'roles' =>  $this->roleTransformer->transformCollection($roles->all())
        ]);
    }
}