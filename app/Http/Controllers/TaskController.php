<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CreateCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Http\Requests\DeleteCommentRequest;
use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Http\Transformers\Tasks\TaskTransformer;
use App\Jobs\Tasks\CreateTask;
use App\Jobs\Tasks\DeleteTask;
use App\Jobs\Tasks\UpdateTask;
use App\Models\Task;
use App\Repositories\TaskRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Behaviours\HandlesCommentsRequests;

class TaskController extends Controller
{
    use HandlesCommentsRequests;

    /**
     * Task repository
     * 
     * @var \App\Repositories\TaskRepository
     */
    private $taskRepo;

    /**
     * Task Transformer
     *
     * @var \App\Http\Transformers\Tasks\TaskTransformer
     */
    private $taskTransformer;

    /**
     * Constructor
     * 
     * @param \App\Repositories\TaskRepository $taskRepo
     */
    public function __construct(TaskRepository $taskRepo, TaskTransformer $taskTransformer)
    {
        $this->taskRepo = $taskRepo;
        $this->taskTransformer = $taskTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $projectId = $request->get('projectId');

        $collection = $this->taskRepo->paginateTasks($projectId, 20);
        
        return $this->respondWithPagination(
            $collection, $this->taskTransformer->transformCollection(
                $collection->items()
            )
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function userTasks(Request $request)
    {
        $collection = $this->taskRepo->userTasks();

        return $this->respond([
            'tasks' =>  $this->taskTransformer->transformCollection(
                $collection->all()
            )
        ]);
    }

    public function userTasksThisWeek(Request $request)
    {
        $startOfWeek = date('Y-m-d', strtotime('this week'));
        $endOfWeek = date('Y-m-d', strtotime('this week + 7 days'));

        $tasks = $this
                ->taskRepo
                ->userTasksInRange($startOfWeek, $endOfWeek)
                ->all()
            ;
        
        $tasksCountEachDay = [];

        $period = new \DatePeriod(
             new \DateTime($startOfWeek),
             new \DateInterval('P1D'),
             new \DateTime($endOfWeek)
        );

        foreach ($period as $dayOfWeek) 
        {

            $tasksForThisDay = array_filter($tasks, function ($task) use ($dayOfWeek) {
                return $task->closed_at == $dayOfWeek->format('Y-m-d');
            });

            $tasksCountEachDay[] = count($tasksForThisDay);
        }

        return $this->respond([
            'tasks' =>  $this->taskTransformer->transformCollection($tasks),
            'tasksCount'    =>  $tasksCountEachDay
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CreateTaskRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTaskRequest $request)
    {
        $this->dispatch(
            app(CreateTask::class, $request->only(['name', 'description', 'projectId', 'endDate']))
        );

        return $this->respondCreated('Task has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = $this->taskRepo->findWith($id, ['comments']);

        return $this->respond([
            'task'    =>  $this->taskTransformer->transform($task)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTaskRequest  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTaskRequest $request, $id)
    {
        $this->dispatch(
            app(UpdateTask::class, 
                array_merge(
                    compact('id'),
                    $request->only(['name', 'description', 'projectId', 'endDate', 'complete'])
                )
            )
        );

        return $this->respondSuccess();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->dispatch(
            app(DeleteTask::class, ['id' => $id])
        );
        return $this->respondSuccess();
    }

    public function addComment(CreateCommentRequest $request, $taskId)
    {
        return $this->performCommentCreate(
            $request, $taskId, Task::class
        );
    }

    public function removeComment(Request $request, $taskId, $commentId)
    {
        return $this->performCommentDelete($commentId);
    }

    public function updateComment(UpdateCommentRequest $request, $taskId, $commentId)
    {
        return $this->performCommentUpdate($request, $commentId);
    }

}
