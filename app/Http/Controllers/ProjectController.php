<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CreateProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Http\Transformers\Projects\ProjectTransformer;
use App\Jobs\Projects\CreateProject;
use App\Jobs\Projects\DeleteProject;
use App\Jobs\Projects\UpdateProject;
use App\Models\Project;
use App\Repositories\ProjectRepository;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Project repository
     * 
     * @var \App\Repositories\ProjectRepository
     */
    private $projectRepo;

    /**
     * User Transformer
     *
     * @var \App\Http\Transformers\Projects\ProjectTransformer
     */
    private $projectTransformer;

    /**
     * Constructor
     * 
     * @param \App\Repositories\ProjectRepository $projectRepo
     */
    public function __construct(ProjectRepository $projectRepo, ProjectTransformer $projectTransformer)
    {
        $this->projectRepo = $projectRepo;
        $this->projectTransformer = $projectTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collection = $this->projectRepo->paginate(20, ['*'], ['client']);

        return $this->respondWithPagination(
            $collection, $this->projectTransformer->transformCollection(
                $collection->items()
            )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CreateProjectRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProjectRequest $request)
    {
        $this->dispatch(
            app(CreateProject::class, $request->only(['name', 'description', 'clientId', 'endDate']))
        );

        return $this->respondCreated('Project has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        return $this->respond([
            'project'    =>  $this->projectTransformer->transform($project)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProjectRequest  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProjectRequest $request, $id)
    {
        $this->dispatch(
            app(UpdateProject::class, $request->only(['id', 'name', 'description', 'clientId', 'endDate']))
        );

        return $this->respondSuccess();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->dispatch(
            app(DeleteProject::class, ['id' => $id])
        );
        return $this->respondSuccess();
    }
}
