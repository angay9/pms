<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CreateCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Http\Transformers\Comments\CommentTransformer;
use App\Models\Comment;
use App\Repositories\CommentRepository;
use Illuminate\Http\Request;


class CommentController extends Controller
{

    private $commentTransformer;
    
    private $commentRepo;

    public function __construct(CommentTransformer $commentTransformer, CommentRepository $commentRepo)
    {
        $this->commentTransformer = $commentTransformer;
        $this->commentRepo = $commentRepo;
    }


    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $commentableId = $request->get('commentable_id');
        $commentableType = $request->get('commentable_type');

        if (!$commentableId || !$commentableType) {
            return $this->respond([]);
        }
        $comments = $this->commentRepo->findComments($commentableId, $commentableType);

        return $this->respond([
            'items' =>  $this->commentTransformer->transformCollection(
                $comments->toArray()
            )
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCommentRequest $request)
    {
        $this->dispatch(
            app(CreateComment::class, $request->only(['body', 'commentableId', 'commentableType']))
        );

        return $this->respondCreated('Comment has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        return $this->respond([
            'comment'    =>  $this->commentTransformer->transform($comment)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCommentRequest $request, $id)
    {
        $this->dispatch(
            app(UpdateComment::class, 
                array_merge(['id' => $id], $request->only(['body', 'commentableId', 'commentableType']))
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->dispatch(
            app(DeleteComment::class, ['id' => $id])
        );
        return $this->respondSuccess();
    }
}
