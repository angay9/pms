<?php

namespace App\Services\Mail;

interface AppMailerInterface 
{
    public function sendInvitationEmail($receiver, $invitationLink);
}