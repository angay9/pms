<?php

namespace App\Services\Mail;

use Illuminate\Mail\Mailer;

class AppMailer implements AppMailerInterface
{
    protected $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendInvitationEmail($receiver, $invitationLink)
    {
        $view = 'emails.invitations.invitation';
        $data = [
            'invitationLink'    =>  $invitationLink
        ];

        $result = $this->mailer->send($view, $data, function ($message) use ($receiver) {
            $message->to($receiver, 'John Doe')->subject('Invitation to the PMS.');
        });
    }
}