<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            Role::ROLE_ADMIN => 1,
            Role::ROLE_USER => 10,
            Role::ROLE_CLIENT => 20
        ];
        
        \DB::transaction(function () use ($roles) {
            foreach ($roles as $role => $times) 
            {
                while ($times--) 
                {
                    $user = factory(App\Models\User::class)->make();
                    $user->save();
                    $user->assignRole($role);
                }
            }
        });
    }
}
