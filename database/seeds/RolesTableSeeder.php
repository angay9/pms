<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            new Role([
                'name'  =>  Role::ROLE_ADMIN
            ]),
            new Role([
                'name'  =>  Role::ROLE_USER
            ]),
            new Role([
                'name'  =>  Role::ROLE_CLIENT
            ])
        ];

        foreach ($roles as $role) 
        {
            $role->save();
        }
    }
}
