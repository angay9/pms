<?php

use App\Models\Project;
use App\Models\Role;
use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::transaction(function () {
            $clients = Role::whereName(Role::ROLE_CLIENT)->firstOrFail()->users;
            $days = rand(20, 365);

            foreach (range(1, 50) as $i) 
            {
                $index = rand(0, count($clients) - 1);
                $client = $clients[$index];

                factory(Project::class)->times(1)->create([
                    'client_id' =>  $client->id,
                    'end_date'  =>  date('Y-m-d', strtotime("+{$days} days"))
                ]);
            }
            
        });
    }
}
